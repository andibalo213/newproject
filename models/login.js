const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const personSchema= new Schema({
    username: String,
    email: String,
    password: String
});
module.exports = mongoose.model('logIn',personSchema);
