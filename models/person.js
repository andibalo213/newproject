const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const personSchema= new Schema({
    username: String,
    email: String,
    phone_number: String,
    password: String
});
module.exports = mongoose.model('Person',personSchema);

//models atau schema berfungsi sebagai databasenya 
//model diexport menggunakan 2 param.pertama adalah nama model dan kedua properties mdoel
//jika ingin ngepost lebih dari field yang ditentukan di personschema maka tidak bisa
//schema berfungsi sebagai validator field,postman key harus sama dengan schema key