const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const usersSchema= new Schema({
    username:{
        type:String
    },
    posts: [{
        type: Schema.Types.ObjectId,
        ref: 'Post' 
    }]
});
module.exports = mongoose.model('User',usersSchema);