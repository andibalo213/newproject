const express =require('express')
const mongoose = require('mongoose')
const router = require('./routes/index')
const port = 3000
var app = express()

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
const devDbUrl = 'mongodb://localhost/test' //connect ke mongo
const mongoDb = process.env.MONGODB_URI || devDbUrl
mongoose.connect(mongoDb,{
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true
})
mongoose.Promise =global.Promise

const db = mongoose.connection
db.on('error',console.error.bind(console,'Mongodb connection error:'))

app.use(express.json())
app.use(express.urlencoded({extended:false}))//buat membaca url di postman
app.use('/api',router) 
// /routes sebagai endpoint induk dari file di routes,
// jadi endpoint berbeda yang digunakan untuk memberi 
// fungsi berbeda di tentukan di file routes.jika endpoint di routes dibuat /users 
// maka otomatis endpoint yagn harus dimasukkana di postman dalah /routes/users
module.exports = app;




