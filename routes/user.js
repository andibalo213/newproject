
var express= require('express')
var router = express.Router()//utk menggunakan http request

const userController = require('../controllers/user')

router.post('/createpost',userController.createPost)
router.get('/',userController.getAll)
router.get('/:username',userController.getPost)

module.exports = router