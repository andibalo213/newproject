const personRouter = require('./person')
const userRouter = require('./user')

var express= require('express')
var router = express.Router()

router.use('/person',personRouter)
router.use('/user',userRouter)

module.exports = router