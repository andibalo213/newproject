
const authorize =require("../helpers/authenticate")
var express= require('express')
var router = express.Router()//utk menggunakan http request

//require mengambil funciton dan property dari fle dituju dan memberinya ke objek yang dideklarasi
const personController= require('../controllers/person')

//req.body mereutrn boejct
//jika mau mengambil spesifik property dary body direturn bukan sebagai object tapi sebagai value
//maka dalam delete atau update eq.body haus di dalam object notation dan property naem

// router.get('/',(req,res) =>{
//     Person.find({},function(err,docs){ //buat filter first param
//         if(err){
//             res.status(422).json({
//                 success:false,
//                 message: 'failed get data'
//             })
//         }else{
//             res.status(200).json({
//                 success:true,
//                 message:'Data collected',
//                 data: docs
//             });
//         }
//     });
// });


//chain of action,kta tdk perlu deklarasi param di routes krn saat dipanggil 
// maka dia akan ke file yang memilik function dan melihat bahwa dia punya param.
//untuk bisa menggunakan body dan function sprti req.body harus selain get request.

router.get('/current',authorize,personController.showUser)
router.get('/person/:user',personController.show); 
router.get('/',personController.showAll); //chain of action
router.get('/hiUser',authorize,personController.hiUser)

// router.post('/',(req,res) =>{
//     console.log(req.body)
//     Person.create(req.body,function(err,person){
        
//         if(err){
//             res.status(422).json({
//                 success:false,
//                 message: 'failed get data'
//             })
//         }else{
//             res.status(200).json({
//                 success:true,
//                 message:'Data collected',
//                 data: person
//             });
//         }
//     });
// });

router.post('/',personController.create)
router.post('/user',personController.createUser)
router.post('/hash',personController.hashme)
router.post('/login',personController.logIn)

//di updateone first parameter adalh sebagai acuan unutk mencari data/obj mana
// yang mau diupdate param kedua adalah property yang diupdate
//param pertama yamg sebagai target harus property specific tidak bisa req.body

// router.put('/',(req,res) =>{
//     console.log(req.body.username);
//     Person.updateOne({username: req.body.username},{username: "hewo"},function(err,person)
//     {
//         if(err){
            
//             res.status(422).json({
//                 success:false,
//                 message: 'failed get data'
//             })
//         }else{
//             res.status(200).json({
//                 success:true,
//                 message:'Data collected',
//                 data: person
//             });
//         }
//     });
// });

router.put('/',personController.update)
//delete menghapus semua obj/data,walaupun param adalh property dari object
//property sebagai acuan dimana obj yang memiliki property dari param tsb 

// router.delete('/',(req,res) =>{
//     Person.deleteOne({username: req.params.user},function(err,person){
//         if(err){
//             res.status(422).json({
//                 success:false,
//                 message: 'failed get data'
//             })
//         }else{
//             res.status(200).json({
//                 success:true,
//                 message:'Data collected',
//                 data: person
//             });
//         }
//     });
// });
router.delete('/:username',personController.delete)


module.exports = router;