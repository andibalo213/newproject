const User =require('../models/users')
const Post =require('../models/post')

module.exports ={      
        createPost(req,res){
        let userName = req.body.username
        let post = req.body.content
        User.findOne({username: userName},function(err,user){
            if(!err){
                    post = new Post({
                        'content':post,
                        'author': user._id
                    })
                
                post.save(function(err){
                    if(err){
                        throw err;
                    }
                    console.log('post created')
                });

                user.posts.push(post._id);
                user.save(function(err){
                    if(err){
                        throw err
                    }
                    console.log('user successfully update')
                    res.status(201).json({sucess:true, message: "success"})
                })
            } else{
                console.log(err)
                res.send(err)
            }
        })
    },
    getAll(req,res){
        User.find({},function(err,person){ //get req tdk bisa make body,diganti menggunakan params/query
                     
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });       
    },
    getPost(req,res){
        userName =req.params.username;
        User.findOne({username:userName})
        .populate({path: 'posts',select:'content'})
        .exec((err,posts)=>{
            //console.log('Populated User' +posts)
            res.send({data:posts, success:true})
        })
  }

}