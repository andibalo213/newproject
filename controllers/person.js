const Person = require('../models/person')
const bcrypt =require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = {
    
    create(req,res){
            Person.create(req.body,function(err,person){
                
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });
    },

    //get req tdk bisa make body,diganti menggunakan params/query
    //untuk menerima variabel dari user bisa memakai req.params atau req. query
    //req. params menggunakan :var di endpoint routes.di postman langsung masukkan value tdk harus titik dua
    // req.query menggunakan ?var=value
    show(req,res){
            Person.findOne({username: req.params.user},function(err,person){ 
                
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });
    },
    showAll(req,res){
            Person.find({},function(err,person){ 
                
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });
    },

    update(req,res){
    Person.updateOne({username: req.body.username},{username: req.body.newusername},function(err,person){
                
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });
    },

    delete(req,res){
            Person.deleteOne({username: req.params.username},function(err,person){
                
            if(err){
                res.status(422).json({
                    success:false,
                    message: 'failed get data'
                })
            }else{
                res.status(200).json({
                    success:true,
                    message:'Data collected',
                    data: person
                });
            }
        });
    },

    hashme(req,res){
        username = req.body.username;
        var salt = bcrypt.genSaltSync(10);
        res.send(bcrypt.hashSync(username, salt));
    },
    createUser(req,res){
          var salt = bcrypt.genSaltSync(10);
          var newUser = new Person({
              username: req.body.username,
              password: bcrypt.hashSync(req.body.password,salt)
          })

       
          newUser.save(function(err){
              if(err) return handleError(err)
              res.send("user has been created")
          })
    },

    logIn(req,res){  
      
        Person.findOne({username: req.body.username},function(err,person){
            var password = bcrypt.compareSync(req.body.password, person.password);
        
            if(person.username == req.body.username && password){
                var token = jwt.sign({_id: person._id}, 'secret') 
                
                res.status(200).json({
                    success:true,
                    message: 'success',
                    token: token
                })

               
                
            }else{
                res.status(200).json({
                    success:false,
                    message:'faield',
                   
                });
            }
        });
    },

    showUser(req,res){  
        console.log(req.user._id)   
        Person.findOne({_id:req.user.id},function(err,data){
            if(err){
                throw err;
            }
            console.log(data)
            res.status(200).json({
                id: data._id,
                username: data.username
            })

            
        })
    },

    hiUser(req,res){
        Person.findOne({_id:req.user.id},function(err,data){
            if(err){
                res.send("error")
            }

            res.send(`Hi ${data.username}`)
        })
    }
}