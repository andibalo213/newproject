const jwt = require('jsonwebtoken');


module.exports = function (req, res, next) {
  var headerToken = req.headers.authorization;
  if (!headerToken) {
    res.send("No Token Found")
  }

  var isUser = jwt.verify(headerToken, "secret");
  if (isUser) {
    req.user = isUser;
    next();
  } else {
    res.status(400).send("Failed To Verify");
  }
}
